/*
15. 3Sum
Medium

Given an array nums of n integers, are there elements a, b, c in nums such that a + b + c = 0? Find all unique triplets in the array which gives the sum of zero.

Notice that the solution set must not contain duplicate triplets.
*/

#include <algorithm>
#include <iostream>
#include <vector>
#include <unordered_set>
#include <unordered_map>

#include "my_lib.h"

std::vector<int> Input() {
    int num, in;
    std::vector<int> vector;

    std::cin >> num;

    for (int i = 0; i < num; ++i) {
        std::cin >> in;
        vector.push_back(in);
    }

    return vector;
}

// time - O(n^2), memory - O(n)

std::vector<std::vector<int>> threeSum(std::vector<int>& nums) {
    std::unordered_map<int, std::vector<size_t>> indexes;
    std::vector<int> xnums;

    for (size_t i = 0; i < nums.size(); ++i) {
        if (!indexes.count(nums[i])) {
            xnums.push_back(nums[i]);
            indexes[nums[i]] = {xnums.size() - 1};
        } else {
            if (indexes[nums[i]].size() < 3) {
                xnums.push_back(nums[i]);
                indexes[nums[i]].push_back(xnums.size() - 1);
            }
        }
    }

    int sum;

    std::unordered_map<int, std::unordered_set<int>> triplets;
    std::vector<int> triplet;

    for (size_t i = 0; i < xnums.size(); ++i) {
        for (size_t j = i + 1; j < xnums.size(); ++j) {
            sum = xnums[i] + xnums[j];

            if (indexes.count(-sum)) {
                if (indexes[-sum].size() < 3) {
                    if (indexes[-sum][0] == i ||
                        indexes[-sum][0] == j) {

                        if (indexes[-sum].size() == 1) {
                            continue;
                        }
                        if (indexes[-sum][1] == i ||
                            indexes[-sum][1] == j) {
                            continue;
                        }
                    }
                }

                triplet = {-sum, xnums[i], xnums[j]};
                std::sort(triplet.begin(), triplet.end());

                if (!triplets.count(triplet[0])) {
                    triplets[triplet[0]] = { triplet[1] };
                } else {
                    triplets[triplet[0]].insert(triplet[1]);
                }
            }
        }
    }

    std::vector<std::vector<int>> result;

    for (auto it = triplets.begin(); it != triplets.end(); ++it) {
        for (auto num_b : it->second) {
            result.push_back({ it->first, num_b, -(it->first + num_b) });
        }
    }

    return result;
}

void Output(const std::vector<std::vector<int>>& matrix) {
    std::cout << "\n";

    for (const auto& vec : matrix) {
        for (const auto& elem : vec) {
            std::cout << elem << " ";
        }
        std::cout << "\n";
    }
}

int main(int argc, char const *argv[]) {
    std::vector<int> nums = Input();

    std::vector<std::vector<int>> result = threeSum(nums);

    Output(result);

    return 0;
}
