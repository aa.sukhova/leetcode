/*
15. 3Sum
Medium

Given an array nums of n integers, are there elements a, b, c in nums such that a + b + c = 0? Find all unique triplets in the array which gives the sum of zero.

Notice that the solution set must not contain duplicate triplets.
*/

#include <algorithm>
#include <vector>

#include "my_lib.h"

std::vector<int> Input() {
    int num, in;
    std::vector<int> vector;

    std::cin >> num;

    for (int i = 0; i < num; ++i) {
        std::cin >> in;
        vector.push_back(in);
    }

    return vector;
}

// time O(n^2), memory O(1 (or n)) <- for sort

std::vector<std::vector<int>> threeSum(std::vector<int>& nums) {
    if (nums.size() < 3) {
        return {};
    }

    std::vector<std::vector<int>> result;
    int left, right, sum;

    std::sort(nums.begin(), nums.end());

    int end = nums.size() - 1;

    while (end > 1) {
        left = 0;
        right = end - 1;

        while (left < right) {
            sum = nums[left] + nums[right] + nums[end];

            if (sum < 0) {
                ++left;
            } else if (sum > 0) {
                -- right;
            } else {
                result.push_back({nums[left], nums[right], nums[end]});

                do {
                    ++left;
                } while (nums[left] == nums[left - 1] && left != right);
            }
        }

        do {
            --end;
        } while (nums[end] == nums[end + 1] && end != 1);
    }

    return result;
}


void Output(const std::vector<std::vector<int>>& matrix) {
    std::cout << "\n";

    for (const auto& vec : matrix) {
        for (const auto& elem : vec) {
            std::cout << elem << " ";
        }
        std::cout << "\n";
    }
}

int main(int argc, char const *argv[]) {
    std::vector<int> nums = Input();

    std::vector<std::vector<int>> result = threeSum(nums);

    Output(result);

    return 0;
}
