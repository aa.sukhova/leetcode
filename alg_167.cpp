/*
167. Two Sum II - Input array is sorted
Easy

Given an array of integers that is already sorted in ascending order, find two numbers such that they add up to a specific target number.

The function twoSum should return indices of the two numbers such that they add up to the target, where index1 must be less than index2.

Note:

    Your returned answers (both index1 and index2) are not zero-based.
    You may assume that each input would have exactly one solution and you may not use the same element twice.
*/

#include <algorithm>
#include <vector>

// time O(n), memory O(1)

std::vector<int> twoSum(std::vector<int>& numbers, int target) {
    if (numbers[0] + numbers[1] > target) {
        return {};
    }
    if (numbers.back() + numbers[numbers.size() - 2] < target) {
        return {};
    }

    int left = 0;
    int right = numbers.size() - 1;

    while (left != right) {
        if (numbers[left] + numbers[right] == target) {
            return {left + 1, right + 1};
        } else if (numbers[left] + numbers[right] < target) {
            ++left;
        } else {
            --right;
        }
    }

    return {};
}
