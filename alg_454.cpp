/*
454. 4Sum II

Given four lists A, B, C, D of integer values, compute how many tuples (i, j, k, l) there are such that A[i] + B[j] + C[k] + D[l] is zero.

To make problem a bit easier, all A, B, C, D have same length of N where 0 ≤ N ≤ 500. All integers are in the range of -228 to 228 - 1 and the result is guaranteed to be at most 231 - 1.
*/

#include<algorithm>
#include<iostream>
#include<unordered_map>

#include "my_lib.h"

std::vector<std::vector<int>> Input() {
    int num, in;

    std::cin >> num;

    std::vector<std::vector<int>> vec_abcd;

    for (int i = 0; i < 4; ++i) {
        for (int j = 0; j < num; ++j) {
            std::cin >> in;
            vec_abcd[i].push_back(in);
        }
    }

    return vec_abcd;
}

int fourSumCount(std::vector<int>& A, std::vector<int>& B, std::vector<int>& C,
                 std::vector<int>& D) {

    std::unordered_map<int, int> sums;
    int sum;

    for (size_t i = 0; i < A.size(); ++i) {
        for (size_t j = 0; j < B.size(); ++j) {
            sum = A[i] + B[j];
            if (!sums.count(sum)) {
                sums[sum] = 0;
            }
            ++sums[sum];
        }
    }

    int count = 0;

    for (size_t i = 0; i < C.size(); ++i) {
        for (size_t j = 0; j < D.size(); ++j) {
            sum = -C[i] - D[j];
            if (sums.count(sum)) {
                count += sums[sum];
            }
        }
    }

    return count;
}

int main() {
    std::vector<std::vector<int>> vecs = Input();

    int sum_count = fourSumCount(vecs[0], vecs[1], vecs[2], vecs[2]);

    std::cout << sum_count << "\n";

    return 0;
}
