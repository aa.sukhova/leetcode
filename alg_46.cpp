
/*
46. Permutations

Given an array nums of distinct integers, return all the possible permutations.
You can return the answer in any order
*/

#include <algorithm>
#include <iostream>
#include <vector>

std::vector<int> Input() {
    int num, in;

    std::cin >> num;
    std::vector<int> input;

    for (int i = 0; i < num; ++i) {
        std::cin >> in;
        input.push_back(in);
    }

    return input;
}

std::vector<std::vector<int>> InternalPermute(int step, std::vector<std::vector<int>>& perms) {
    std::vector<std::vector<int>> mult;

    for (auto& perm : perms) {
        mult.push_back(perm);

        int from = perm.size() - step;
        for (int i = from + 1; i < perm.size(); ++i) {
            std::swap(perm[from], perm[i]);
            mult.push_back(perm);
        }
    }

    return mult;
}

std::vector<std::vector<int>> Permute(std::vector<int>& nums) {
    std::vector<std::vector<int>> result = {nums};

    std::sort(result[0].begin(), result[0].end());

    for (int i = nums.size(); i >= 0; --i) {
        result = InternalPermute(i, result);
    }

    return result;
}

void Output(const std::vector<std::vector<int>>& matrix) {
    std::cout << "\n";

    for (const auto& vec : matrix) {
        for (const auto& elem : vec) {
            std::cout << elem << " ";
        }
        std::cout << "\n";
    }
}

int main() {
    std::vector<int> numbers = Input();

    std::vector<std::vector<int>> result = Permute(numbers);

    Output(result);

    return 0;
}
