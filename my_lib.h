#include <iostream>
#include <vector>

template <class T>
void PrintVec(std::vector<T>& vec) {
    for (auto& elem : vec) {
        std::cout << elem << " ";
    }
    std::cout << "\n";
}

template <class T>
void PrintMatrix(std::vector<std::vector<T>>& matrix) {
    for (const auto& vec : matrix) {
        for (const auto& elem : vec) {
            std::cout << elem << " ";
        }
        std::cout << "\n";
    }
}
